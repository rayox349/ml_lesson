import pandas as pd
import matplotlib.pyplot as plt


def plot_origin_data(data_df: pd.DataFrame,
                     x: str = 'sugars',
                     y: str = 'rating'):
    data_df.plot.scatter(x=x, y=y)

    plt.xlim([(data_df[x].min() - 1), (data_df[x].max() + 1)])
    plt.ylim([(data_df[y].min() - 1), (data_df[y].max() + 1)])
    plt.plot([(data_df[x].min() - 1), (data_df[x].max() + 1)],
             [(data_df[y].min() - 1), (data_df[y].max() + 1)], '--', color='#DD5511')

    plt.show()
    # 共變異數
    s_xy = (data_df[x] - data_df[x].mean()) * (data_df[y] - data_df[y].mean()) / len(data_df)
    print(f"相關係數:{s_xy.sum() / (data_df[x].std() * data_df[y].std()).sum()}")


def plot_z_score_data(data_df: pd.DataFrame,
                      x: str = 'sugars',
                      # x: str = 'rating',
                      y: str = 'rating',
                      ):
    data_df[f'z-score_{x}'] = (data_df[x] - data_df[x].mean()) / data_df[x].std()
    data_df[f'z-score_{y}'] = (data_df[y] - data_df[y].mean()) / data_df[y].std()

    data_df.plot.scatter(x=f'z-score_{x}',
                         y=f'z-score_{y}')
    plt.xlim([-2, 2])
    plt.ylim([-2, 2])
    plt.plot([-2, 2], [-2, 2], '--', color='#225511')
    plt.show()

    print(f"相關係數:{(data_df[f'z-score_{x}'] * data_df[f'z-score_{y}']).mean()}")


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")

    plot_origin_data(data_df)
    plot_z_score_data(data_df)


if __name__ == '__main__':
    main()
