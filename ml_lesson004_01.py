import pandas as pd
import numpy as np


def group_by_column(data_df: pd.DataFrame,
                    column_name: str = 'A'
                    ):
    """
        pd.cut()
    """
    auto_group = pd.cut(x=data_df[column_name],
                        bins=6, )
    print(f'auto_group\n{auto_group}')
    print(pd.value_counts(auto_group))
    print('-' * 100)

    group_with_array = pd.cut(x=data_df[column_name],
                              bins=[0, 1, 2, 3, 4, 5, 10])
    print(f'group with bin array\n{group_with_array}')
    print(pd.value_counts(group_with_array))
    print('-' * 100)

    group_with_array_and_label = pd.cut(x=data_df[column_name],
                                        bins=[0, 1, 2, 3, 4, 5, 6],
                                        labels=['G1', 'G2', 'G3', 'G4', 'G5', 'G6'], )
    print(f'group with bin array and label\n{group_with_array_and_label}')
    print(pd.value_counts(group_with_array_and_label))
    print('-' * 100)

    data_df['group_as'] = group_with_array_and_label
    print(data_df)


def main():
    data_df = pd.DataFrame(columns=['A', 'B', 'C', 'D'],
                           data=np.array([
                               [1, 1, 1, 1],
                               [2, 2, np.nan, 2],
                               [3, 3, 3, np.nan],
                               [4, np.nan, np.nan, np.nan],
                               [5, np.nan, np.nan, 5],
                               [6, 6, np.nan, 6],
                           ]))
    group_by_column(data_df)


if __name__ == '__main__':
    main()
