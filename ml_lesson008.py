import pandas as pd
from sklearn import svm
from sklearn.model_selection import cross_val_score
import numpy as np


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    column_x = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
                'weight', 'cups']
    column_y = ['rating']
    data_feature = data_df[column_x].values
    data_label = data_df[column_y].values

    svm_model = svm.SVR()
    svm_model.fit(data_feature, data_label.ravel())

    scores = cross_val_score(svm_model, data_feature, data_label.ravel(),
                             scoring="neg_mean_squared_error", cv=10)
    evaluate_rmse_scores = np.sqrt(-scores)

    print("Scores:", evaluate_rmse_scores)
    print("Mean:", evaluate_rmse_scores.mean())
    print("Standard deviation:", evaluate_rmse_scores.std())


if __name__ == '__main__':
    main()
