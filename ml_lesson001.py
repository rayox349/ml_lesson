import pandas as pd


def show_complete_row(data_df: pd.DataFrame):
    pd.set_option('display.max_rows', None)
    print(data_df)
    pd.reset_option('display.max_rows')


def show_complete_column(data_df: pd.DataFrame):
    pd.set_option('display.width', None)
    print(data_df)
    pd.reset_option('display.width')


def show_head(data_df: pd.DataFrame, num_rows: int = 5):
    print(data_df.head(num_rows))


def show_tail(data_df: pd.DataFrame, num_rows: int = 5):
    print(data_df.tail(num_rows))


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    show_head(data_df)
    show_tail(data_df)
    show_complete_row(data_df)
    show_complete_column(data_df)


if __name__ == '__main__':
    main()
