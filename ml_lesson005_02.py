import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def df_correlation_matrix(data_df: pd.DataFrame, column_list: list):
    df_corr = data_df[column_list].corr()
    mask = np.zeros_like(df_corr)
    mask[np.tril_indices_from(mask)] = 1

    sns.heatmap(df_corr, cbar=False, square=True,
                cmap='rainbow',
                linewidths=1,
                )
    plt.xticks(rotation=45)
    plt.show()

    sns.heatmap(df_corr, cbar=True, annot=True, square=True, fmt='.2f',
                annot_kws={'size': 8, 'weight': 'bold'},
                yticklabels=column_list, xticklabels=column_list, cmap='rainbow',
                linewidths=1,
                mask=mask)
    plt.xticks(rotation=45)
    plt.show()
    print(df_corr['rating'].sort_values(ascending=False))


def np_correlation_matrix(data_df: pd.DataFrame, column_list: list):
    cm = np.corrcoef(data_df[column_list].values.T)
    mask = np.zeros_like(cm)
    mask[np.tril_indices_from(mask)] = True
    sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f',
                annot_kws={'size': 8, 'weight': 'bold'},
                yticklabels=column_list, xticklabels=column_list, cmap='rainbow',
                linewidths=1,
                mask=mask.T)
    plt.xticks(rotation=45)
    plt.show()
    print('各欄位與[rating]欄位相關性')
    print(pd.concat((pd.DataFrame(column_list[:], columns=['特徵名稱']),
                     pd.DataFrame(cm[:][len(column_list) - 1], columns=['相關係數'])), axis=1)
          .sort_values(by='相關係數', ascending=False)[:])


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    # columns = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
    #            'weight', 'cups', 'rating']
    columns = list(data_df.describe().columns)
    df_correlation_matrix(data_df, columns)
    np_correlation_matrix(data_df, columns)


if __name__ == '__main__':
    main()
