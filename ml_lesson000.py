import pandas as pd
from pandas_profiling import ProfileReport


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    ProfileReport(data_df[list(data_df.describe().columns)],
                  title="Pandas Profiling Report").to_file('report.html')


if __name__ == "__main__":
    main()
