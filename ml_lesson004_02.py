import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def group_by_column(data_df: pd.DataFrame,
                    column_name: str = 'mfr',
                    target_name: str = 'G'):
    """
        df.groupby()
    """
    af_group = data_df.groupby(by=column_name, as_index=False)
    # af_group = data_df.groupby(by=column_name, as_index=True)
    # print(list(af_group))

    print(f'\ngroups(index): \n{af_group.groups}')
    print(f'\nget_groups(dataframe) \n{af_group.get_group(target_name)}')

    print(af_group.count())
    print('-' * 100)


def remove_by_target_name(data_df: pd.DataFrame,
                          column_name: str = 'mfr',
                          target_name: str = 'R'):
    af_group = data_df.groupby(by=column_name,
                               as_index=False)
    target_group = af_group.get_group(target_name)
    print(f"目標所在索引值:{target_group.index}, {list(target_group.index)}")

    af_drop_target = data_df.drop(index=target_group.index)
    print(f'原始資料筆數:{len(data_df)},刪除目標後筆數:{len(af_drop_target)}')
    print(f"{list(af_group.groups)}\n"
          f"{list(af_drop_target.groupby(by=column_name, as_index=False).groups)}")

    sns.countplot(y='mfr',
                  data=data_df,
                  order=data_df['mfr'].value_counts().index,
                  lw=2,
                  ec='black')
    plt.show()
    sns.countplot(y='mfr',
                  data=af_drop_target,
                  order=data_df['mfr'].value_counts().index,
                  lw=2,
                  ec='black')
    plt.show()


def sample_with_group_by(data_df: pd.DataFrame,
                         column_name: str = 'mfr',
                         ):
    af_group = data_df.groupby(by=column_name,
                               as_index=False)
    print(af_group.sample(n=1,
                          random_state=1111))

    print(af_group.sample(frac=0.1,
                          random_state=1111))


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    pd.set_option('display.width', None)
    group_by_column(data_df)
    remove_by_target_name(data_df)
    sample_with_group_by(data_df)


if __name__ == '__main__':
    main()
