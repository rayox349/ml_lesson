# ML_lesson

## 環境建制

[安裝流程參考](https://hackmd.io/@rayox349/SkGcDa_tu)  
**以 conda 建立 python 3.7+ 環境**
```shell script
conda create --name ml_lesson python=3.7.11
```

## [ml_lesson00](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson000.ipynb)

### 快速分析資料

環境需要安裝  
`pip install pandas-profiling[notebook]`

- ProfileReport()

## [ml_lesson001](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson001.ipynb)

### 顯示

- pandas.set_option()
- df.head()
- df.tail()

## [ml_lesson002](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson002.ipynb)

### pandas 條件搜尋運用

- df.query()
- boolean mask
- df.where()
- np.where()
- df.at[]
- df.loc[]
- df.idxmin()
- df.nsmallest()
- df.idxmax()
- df.nlargest()

## [ml_lesson003_01](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson003_01.ipynb)

### pandas 遺漏值

- df.isnull()
- df.dropna()

## [ml_lesson003_02](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson003_02.ipynb)

### pandas 遺漏值處理-補值

- SimpleImputer()
- df.interpolate()
- df.fillna()
- df.replace()

## [ml_lesson004_01](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson004_01.ipynb)

### pandas 資料分組-數值

- pandas.cut()

## [ml_lesson004_02](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson004_02.ipynb)

### pandas 資料分組-類別

- df.groupby()
- df.sample

| Before        | After         |
|:-------------:|:-------------:|
| <img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson004_02_before.png width = "250" height = "250" alt="" align=center />|<img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson004_02_after.png width = "250" height = "250" alt="" align=center />|


## [ml_lesson005_01](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson005_01.ipynb)

### 相關係數

- df.plot.scatter()

| Original      | Z-score       |
|:-------------:|:-------------:|
| <img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson005_01_original.png width = "250" height = "250" alt="" align=center />|<img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson005_01_z-score.png width = "250" height = "250" alt="" align=center />|


## [ml_lesson005_02](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson005_02.ipynb)

### 相關矩陣

- df.corr()

| Without text  | Lower         | Upper         |
|:-------------:|:-------------:|:-------------:|
| <img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson005_02_corr_without_number.png width = "225" height = "225" alt="" align=center />|<img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson005_02_corr_lower.png width = "225" height = "225" alt="" align=center />|<img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson005_02_corr_upper.png width = "225" height = "225" alt="" align=center />


## [ml_lesson006](https://gitlab.com/rayox349/ml_lesson/-/blob/main/ml_lesson006.ipynb)

### 模型預測

- train_test_split
- StandardScaler()
- svm.SVR()

| Original      | StandardScaler|
|:-------------:|:-------------:|
| <img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson006_original.png width = "250" height = "250" alt="" align=center />|<img src=https://gitlab.com/rayox349/ml_lesson/-/raw/main/image/ml_lesson006_standard.png width = "250" height = "250" alt="" align=center />|


## ml_lesson007_01

### 參數最佳化搜尋

- 網格搜尋 (GridSearchCV)

## ml_lesson007_01

### 參數最佳化搜尋

- 隨機搜尋 (RandomizedSearchCV)

## ml_lesson008

### 資料抽樣驗證

- 交叉驗證 (cross_val_score)

## ml_lesson009

### 團結的力量

- 集成學習 (VotingClassifier)

## ml_lesson010

### 借我參考一下

- 堆疊法 (StackingClassifier)