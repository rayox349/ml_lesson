from dataclasses import dataclass
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn import neighbors
from sklearn import svm
from sklearn import tree
from sklearn import linear_model
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
import warnings

warnings.filterwarnings('ignore')


@dataclass
class DataGroup(object):
    train_data_feature: pd.DataFrame
    train_data_label: pd.DataFrame
    test_data_feature: pd.DataFrame
    test_data_label: pd.DataFrame


def get_classification_data_df(data_df: pd.DataFrame,
                               target_column_name='rating') -> pd.DataFrame:
    class_data_df = data_df.copy()
    situation_mask = class_data_df[target_column_name] >= class_data_df[target_column_name].mean()
    class_data_df.loc[situation_mask, target_column_name] = '1'
    class_data_df.loc[~situation_mask, target_column_name] = '0'
    pd.set_option('display.max_rows', None)
    return class_data_df


def get_spilt_data(data_df: pd.DataFrame,
                   feature_column: list,
                   label_column: list) -> 'DataGroup':
    train_feature, test_feature, train_label, test_label = train_test_split(data_df[feature_column],
                                                                            data_df[label_column],
                                                                            test_size=0.2,
                                                                            random_state=0)
    return DataGroup(train_feature.values,
                     train_label.values.ravel(),
                     test_feature.values,
                     test_label.values.ravel())


def get_svm_model(data: DataGroup) -> svm.SVC:
    svm_model = svm.SVC(probability=True)
    svm_model.fit(data.train_data_feature, data.train_data_label)
    return svm_model


def get_knn_model(data: DataGroup) -> neighbors.KNeighborsClassifier:
    knn_model = neighbors.KNeighborsClassifier(n_neighbors=5)
    knn_model.fit(data.train_data_feature, data.train_data_label)
    return knn_model


def get_tree_model(data: DataGroup) -> tree.DecisionTreeClassifier:
    tree_model = tree.DecisionTreeClassifier()
    tree_model.fit(data.train_data_feature, data.train_data_label)
    return tree_model


def get_nb_model(data: DataGroup) -> naive_bayes.GaussianNB:
    nb_model = naive_bayes.GaussianNB()
    nb_model.fit(data.train_data_feature, data.train_data_label)
    return nb_model


def get_logistic_model(data: DataGroup) -> linear_model.LogisticRegression:
    # sc = StandardScaler()
    # train_feature_std = sc.fit_transform(data.train_data_feature)
    logistic_model = linear_model.LogisticRegression()
    # logistic_model.fit(train_feature_std, data.train_data_label)
    logistic_model.fit(data.train_data_feature, data.train_data_label)
    return logistic_model


def get_voting_model(data: DataGroup,
                     estimators: list,
                     voting: str = 'soft') -> VotingClassifier:
    # sc = StandardScaler()
    # train_feature_std = sc.fit_transform(data.train_data_feature)
    voting_model = VotingClassifier(estimators=estimators, voting=voting)
    # voting_model.fit(train_feature_std, data.train_data_label)
    voting_model.fit(data.train_data_feature, data.train_data_label)
    return voting_model


def show_tree_feature_importance(feature_column: list, tree_model: tree.DecisionTreeClassifier):
    tree_feature_importance = pd.DataFrame(
        {'feature': feature_column,
         'feature_importance': tree_model.feature_importances_.tolist()
         }
    ).sort_values(by=['feature_importance'], ascending=False)
    print(f'Decision feature importance\n{tree_feature_importance}\n{"-" * 100}')


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    feature_column = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
                      'weight', 'cups']
    label_column = ['rating']
    class_data_df = get_classification_data_df(data_df)
    data = get_spilt_data(data_df=class_data_df,
                          feature_column=feature_column,
                          label_column=label_column)

    svm_model = get_svm_model(data=data)
    # print(accuracy_score(data.test_data_label,
    #                      svm_model.predict(data.test_data_feature)))

    knn_model = get_knn_model(data=data)
    # print(accuracy_score(data.test_data_label,
    #                      knn_model.predict(data.test_data_feature)))

    tree_model = get_tree_model(data=data)
    # print(accuracy_score(data.test_data_label,
    #                      tree_model.predict(data.test_data_feature)))

    nb_model = get_nb_model(data=data)
    # print(accuracy_score(data.test_data_label,
    #                      nb_model.predict(data.test_data_feature)))

    logistic_model = get_logistic_model(data=data)
    # sc = StandardScaler()
    # test_feature_std = sc.fit_transform(data.test_data_feature)
    # logistic_model_result = np.argmax(
    #     logistic_model.predict_proba(test_feature_std),
    #     axis=1
    # ).astype('str')

    # print(accuracy_score(data.test_data_label,
    #                      logistic_model_result))

    voting_model = get_voting_model(data=data,
                                    estimators=[
                                        ('svm', svm_model),
                                        ('knn', knn_model),
                                        ('tree', tree_model),
                                        ('nb', nb_model),
                                        ('logistic', logistic_model)
                                    ],
                                    voting='soft'  # voting='hard'
                                    )
    # print(accuracy_score(data.test_data_label,
    #                      voting_model.predict(test_feature_std)))
    for estimators in [svm_model, knn_model, tree_model, nb_model, logistic_model, voting_model]:
        print(f'{estimators.__class__.__name__}:{estimators.score(data.test_data_feature, data.test_data_label)}')


if __name__ == "__main__":
    main()
