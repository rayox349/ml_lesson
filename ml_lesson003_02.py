import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer


def preprocess_null_values_with_imputer(data_df: pd.DataFrame):
    """
    SimpleImputer
        strategy = ["mean", "median", "most_frequent", "constant"]
    """
    imp_mean = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp_mean.fit(data_df)
    print(imp_mean.statistics_)
    print(imp_mean.transform(data_df))


def preprocess_null_values_with_interpolate(data_df: pd.DataFrame):
    """
    df.interpolate()
        method = 'linear', 'akima', 'barycentric', 'cubic', 'cubispline'
                 'from_derivates', 'index', 'krogh', 'nearest'
                'pad', 'pchip', 'piecewise_polynomial', 'polynomial', 'quadric'
                'slinear', 'spline', 'time', 'zero', 'bfill'
                'pad', 'ffill'
    """
    print(data_df.interpolate(method='linear'))


def preprocess_null_values_with_replace(data_df: pd.DataFrame):
    """
        df.replace()

    """
    print(data_df.replace(to_replace=np.nan, value=99))


def preprocess_null_values_with_fillna(data_df: pd.DataFrame):
    """
        df.fillna()
    """
    # 依照每列中位數補值
    # print(data_df['B'].fillna(data_df['B'].median()))
    print(data_df.fillna(data_df.median()))


def main():
    data_df = pd.DataFrame(columns=['A', 'B', 'C', 'D'],
                           data=np.array([
                               [1, 1, 1, 1],
                               [2, 2, np.nan, 2],
                               [3, 3, 3, np.nan],
                               [np.nan, np.nan, np.nan, np.nan],
                               [5, np.nan, np.nan, 5],
                               [6, 6, np.nan, 6],
                           ]))
    preprocess_null_values_with_imputer(data_df)
    preprocess_null_values_with_interpolate(data_df)
    preprocess_null_values_with_replace(data_df)
    preprocess_null_values_with_fillna(data_df)


if __name__ == '__main__':
    main()
