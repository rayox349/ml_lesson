import pandas as pd
import numpy as np
import sys


def find_target(data_df: pd.DataFrame,
                target_value=70):
    """
        df.query()
        boolean mask
        df.where()
        np.where()
    """

    print(data_df.query("calories == @target_value"))

    print(data_df[data_df['calories'] == target_value])
    print(data_df[data_df.calories == target_value])

    assert data_df[data_df['calories'] == target_value].equals(data_df[data_df.calories == target_value])

    print(data_df.where(data_df != -1, 'value_match'))
    np.set_printoptions(threshold=sys.maxsize)
    print(np.where(np.array(data_df) == target_value, 'value_match', 1))

    # 多條件搜尋
    print(data_df.query("calories == @target_value | mfr == 'N'"))
    print(data_df[(data_df['calories'] == target_value) | (data_df['mfr'] == 'N')])


def get_target_column(data_df: pd.DataFrame,
                      column_name: str = 'calories'):
    print('指定欄位 calories 的資料')
    print(data_df[[column_name]].head())

    print('去除 calories 欄位資料')
    print(data_df.drop(columns=column_name).head())


def set_value(data_df: pd.DataFrame,
              column_name: str = 'calories',
              situation: int = 70,
              assign_value: int = 7777):
    """
        boolean mask
        df.at[]
        df.loc[]
    """

    data_df_copy = data_df.copy()
    data_df_copy[data_df_copy[column_name] == situation] = assign_value

    data_df_with_at = data_df.copy()
    for index, row in data_df_with_at.iterrows():
        if row[column_name] == situation:
            data_df_with_at.at[index, column_name] = assign_value

    data_df_with_loc = data_df.copy()
    data_df_with_loc.loc[data_df_with_loc[column_name] == situation, column_name] = assign_value

    print(data_df_with_at[column_name] == data_df_with_loc[column_name])


def find_minimum(data_df: pd.DataFrame,
                 column_name: str = 'calories'):
    """
        ** 需要注意欄位需為可比較大小的數值 **
        df.idxmin()
        df.nsmallest()
    """
    # print(data_df.idxmin())  # 因為資料中有不能比較大小的欄位所以無法直接透過這個方式找索引值

    print(f'Minimum {column_name} index: {data_df[column_name].idxmin()}')
    print(f'Minimum {column_name}: {data_df.loc[data_df[column_name].idxmin()][column_name]}')

    # 你也可以透過以下 boolean 方式來找最小值的結果
    # print(f'calories: {data_df[data_df[column_name] == data_df[column_name].min()][column_name].values[0]}')
    print(data_df.nsmallest(n=5,
                            columns=column_name))


def find_maximum(data_df: pd.DataFrame,
                 column_name: str = 'calories'):
    """
        ** 需要注意欄位需為可比較大小的數值 **
        df.idxmax()
        df.nlargest()
    """

    print(f'Maximum {column_name} index: {data_df[column_name].idxmax()}')
    print(f'Maximum {column_name}: {data_df.loc[data_df[column_name].idxmax()][column_name]}')

    # 你也可以透過以下 boolean 方式來找最大值的結果
    # print(f'calories: {data_df[data_df[column_name] == data_df[column_name].max()][column_name].values[0]}')
    print(data_df.nlargest(n=5,
                           columns=column_name))


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    pd.set_option('display.width', None)
    find_target(data_df)
    get_target_column(data_df)
    set_value(data_df)
    find_minimum(data_df)
    find_maximum(data_df)


if __name__ == '__main__':
    main()
