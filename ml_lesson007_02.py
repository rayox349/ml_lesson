import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn import svm
import numpy as np


def show_svm_results(
        svm_model,
        train_feature: pd.DataFrame,
        test_feature: pd.DataFrame,
        train_label: pd.DataFrame,
        test_label: pd.DataFrame):
    svm_model.fit(train_feature, train_label)
    print(f'train_score: {svm_model.score(train_feature, train_label)}')
    print(f'test_score: {svm_model.score(test_feature, test_label)}')
    print(svm_model.predict(test_feature))
    print(test_label)


def find_best_parameters(
        svm_model,
        train_feature: pd.DataFrame,
        train_label: pd.DataFrame, ):
    param_grid = [
        {'kernel': ['linear', 'poly', 'rbf', 'sigmoid', ],
         'gamma': ['scale', 'auto'],
         'C': list(range(1, 5))
         },
    ]
    search_best_param = RandomizedSearchCV(svm_model,
                                           param_distributions=param_grid,
                                           cv=5,
                                           scoring='neg_mean_squared_error',
                                           random_state=9999)
    search_best_param.fit(train_feature, train_label)
    search_record = search_best_param.cv_results_
    for mean_score, params in zip(search_record["mean_test_score"], search_record["params"]):
        print(np.sqrt(-mean_score), params)

    print(f'最佳參數: {search_best_param.best_params_}')
    return search_best_param.best_estimator_


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    column_x = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
                'weight', 'cups']
    column_y = ['rating']

    train_feature, test_feature, train_label, test_label = train_test_split(data_df[column_x],
                                                                            data_df[column_y],
                                                                            test_size=0.2,
                                                                            random_state=0)
    svm_model = svm.SVR()
    show_svm_results(svm_model,
                     train_feature.values,
                     test_feature.values,
                     train_label.values.ravel(),
                     test_label.values.ravel())

    svm_model = find_best_parameters(svm_model,
                                     train_feature.values,
                                     train_label.values.ravel())
    show_svm_results(svm_model,
                     train_feature.values,
                     test_feature.values,
                     train_label.values.ravel(),
                     test_label.values.ravel())


if __name__ == '__main__':
    main()
