import pandas as pd
import numpy as np


def statistic_null_column(data_df: pd.DataFrame):
    """
        df.isnull()
    """
    print(f'各欄位中出現nan的筆數:\n{data_df.isnull().sum()}')
    print(f'指定欄位中出現nan的筆數: {data_df["D"].isnull().sum()}')


def drop_null(data_df: pd.DataFrame):
    """
        df.dropna()
    """
    print(f'刪除前資料筆數: {len(data_df)}')
    after_data_df = data_df.dropna()
    print(f'刪除後資料筆數: {len(after_data_df)}')
    print(after_data_df)

    print('------------------------------------------------axis=1')
    print(f'刪除前欄位筆數: {len(data_df.columns)}')
    after_data_df = data_df.dropna(axis=1)
    print(f'刪除後欄位筆數: {len(after_data_df.columns)}')
    print(after_data_df)

    print('------------------------------------------------how=all')
    print(f'刪除前資料筆數: {len(data_df)}')
    after_data_df = data_df.dropna(how='all')  # 全部皆為nan才會被刪除
    print(f'刪除後資料筆數: {len(after_data_df)}')
    print(after_data_df)

    print('------------------------------------------------thresh')
    print(f'刪除前資料筆數: {len(data_df)}')
    after_data_df = data_df.dropna(thresh=3)  # 資料中至少3個不等於 nan 才會被保留
    print(f'刪除後資料筆數: {len(after_data_df)}')
    print(after_data_df)

    print('------------------------------------------------subset')
    print(f'刪除前資料筆數: {len(data_df)}')
    after_data_df = data_df.dropna(subset=['A'])  # 指定欄位存在 nan 才會被刪除
    print(f'刪除後資料筆數: {len(after_data_df)}')
    print(after_data_df)


def main():
    data_df = pd.DataFrame(columns=['A', 'B', 'C', 'D'],
                           data=np.array([
                               [1, 1, 1, 1],
                               [2, 2, np.nan, 2],
                               [3, 3, 3, np.nan],
                               [np.nan, np.nan, np.nan, np.nan],
                               [5, np.nan, np.nan, 5],
                               [6, 6, np.nan, 6],
                           ]))
    print(data_df)
    print(f'各欄位中不為nan的筆數:\n{data_df.count()}')
    statistic_null_column(data_df)
    drop_null(data_df)


if __name__ == '__main__':
    main()
