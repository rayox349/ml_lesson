import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt


# 根據 sklearn class RegressorMixin
# score -> r2_score
def show_svm_results(
        svm_model,
        train_feature: pd.DataFrame,
        test_feature: pd.DataFrame,
        train_label: pd.DataFrame,
        test_label: pd.DataFrame):
    print(f'train_score: {svm_model.score(train_feature, train_label)}')
    print(f'test_score: {svm_model.score(test_feature, test_label)}')


def plot_residual(svm_model,
                  train_feature: pd.DataFrame,
                  test_feature: pd.DataFrame,
                  train_label: pd.DataFrame,
                  test_label: pd.DataFrame):
    train_predictions = svm_model.predict(train_feature)
    test_predictions = svm_model.predict(test_feature)
    range_x_max = max(max(abs(train_label - train_predictions)), max(abs(test_label - test_predictions)))
    range_x_min = min(min(abs(train_label - train_predictions)), min(abs(test_label - test_predictions)))
    plt.scatter(train_predictions, train_predictions - train_label,
                c='blue', marker='o', label='Training data',
                alpha=0.6)
    plt.scatter(test_predictions, test_predictions - test_label, c='red', marker='x', label='Testing data')
    plt.xlabel('Predicted values')
    plt.ylabel('Residuals')
    plt.legend(loc='upper left')
    plt.hlines(y=0, xmin=range_x_min, xmax=range_x_max, lw=2, color='darkgreen')
    plt.xlim([range_x_min, range_x_max])
    plt.show()


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    column_x = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
                'weight', 'cups']
    column_y = ['rating']

    train_feature, test_feature, train_label, test_label = train_test_split(data_df[column_x],
                                                                            data_df[column_y],
                                                                            test_size=0.2,
                                                                            random_state=0)
    svm_model = svm.SVR()
    svm_model.fit(train_feature.values, train_label.values.ravel())
    show_svm_results(svm_model,
                     train_feature.values,
                     test_feature.values,
                     train_label.values.ravel(),
                     test_label.values.ravel())
    plot_residual(svm_model,
                  train_feature.values,
                  test_feature.values,
                  train_label.values.ravel(),
                  test_label.values.ravel())

    sc = StandardScaler()
    train_feature_std = sc.fit_transform(train_feature)
    test_feature_std = sc.fit_transform(test_feature)
    svm_model.fit(train_feature_std, train_label.values.ravel())
    show_svm_results(svm_model,
                     train_feature_std,
                     test_feature_std,
                     train_label.values.ravel(),
                     test_label.values.ravel())
    plot_residual(svm_model,
                  train_feature_std,
                  test_feature_std,
                  train_label.values.ravel(),
                  test_label.values.ravel())


if __name__ == '__main__':
    main()
