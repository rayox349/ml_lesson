from dataclasses import dataclass
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn import neighbors
from sklearn import svm
from sklearn import tree
from sklearn import linear_model
from sklearn.ensemble import StackingClassifier
import pandas as pd

import warnings

warnings.filterwarnings('ignore')


@dataclass
class DataGroup(object):
    train_data_feature: pd.DataFrame
    train_data_label: pd.DataFrame
    test_data_feature: pd.DataFrame
    test_data_label: pd.DataFrame


def get_classification_data_df(data_df: pd.DataFrame,
                               target_column_name='rating') -> pd.DataFrame:
    class_data_df = data_df.copy()
    situation_mask = class_data_df[target_column_name] >= class_data_df[target_column_name].mean()
    class_data_df.loc[situation_mask, target_column_name] = '1'
    class_data_df.loc[~situation_mask, target_column_name] = '0'
    pd.set_option('display.max_rows', None)
    return class_data_df


def get_spilt_data(data_df: pd.DataFrame,
                   feature_column: list,
                   label_column: list) -> 'DataGroup':
    train_feature, test_feature, train_label, test_label = train_test_split(data_df[feature_column],
                                                                            data_df[label_column],
                                                                            test_size=0.2,
                                                                            random_state=0)
    return DataGroup(train_feature.values,
                     train_label.values.ravel(),
                     test_feature.values,
                     test_label.values.ravel())


def main():
    data_df = pd.read_csv('./data/original_cereal.csv', encoding="UTF-8")
    feature_column = ['calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins', 'shelf',
                      'weight', 'cups']
    label_column = ['rating']
    class_data_df = get_classification_data_df(data_df)
    data = get_spilt_data(data_df=class_data_df,
                          feature_column=feature_column,
                          label_column=label_column)

    estimators = [
        ('logistic', linear_model.LogisticRegression()),
        ('svm', svm.SVC(probability=True)),
        ('knn', neighbors.KNeighborsClassifier(n_neighbors=5)),
        ('tree', tree.DecisionTreeClassifier()),
        ('nb', naive_bayes.GaussianNB()),
    ]

    stack_model = StackingClassifier(estimators=estimators)
    stack_model.fit(data.train_data_feature, data.train_data_label)

    print(stack_model.score(data.test_data_feature, data.test_data_label))


if __name__ == '__main__':
    main()
